#ifndef CONSOLE_H
#define CONSOLE_H
void console_init(void);
int putchar(int c);
int puts(const char *c);
void console_set_irq_en(bool rx_en, bool tx_en);
#endif
