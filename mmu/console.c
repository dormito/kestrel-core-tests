#include <stdint.h>
#include <stdbool.h>
#define UART_EV_TX 0x1
#define UART_EV_RX 0x2

#define FIFO    0x0
#define TX_ST   0x1
#define EV_PEND 0x4
#define EV_EN   0x5

void console_init()
{
    static volatile uint32_t *uart = (volatile uint32_t *)0xc0002000;
    uart[4] = uart[4];
    uart[5] = UART_EV_TX | UART_EV_RX;
}

int putchar(int c)
{
    static volatile uint32_t *uart = (volatile uint32_t *)0xc0002000;
    //static volatile uint32_t *fifo = (volatile uint32_t *)0xc0002000;
    static volatile const uint32_t *txstat = (volatile const uint32_t *)0xc0002004;
    while (*txstat) ;
    //    *fifo = c;

    uart[FIFO] = c;

    uart[4] = UART_EV_TX;
    return 0;
}

int puts(const char *c)
{
    int i = 0;
    for (i = 0; c[i]; i++)
    {
        putchar(c[i]);
        if (c[i] == '\n')
            putchar('\r');
    }
    return i;
};
/* on kestrel this is a no-op i guess */
void console_set_irq_en(bool rx_en, bool tx_en)
{
}
